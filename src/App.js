import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class App extends Component {
  constructor (props) {
    super(props)
    this.handleButton2 = this.handleButton2.bind(this)
    this.state = {
      count: 0
    }
  }
  handleButton () {
    this.setState(prevState => ({
      count: prevState.count + 1
    }))
  }
  handleButton2 () {
    this.setState(prevState => ({
      count: prevState.count + 1
    }))
  }
  // 不适用bind 必须的用箭头函数
  handleClick = () => {
    this.setState(prevState => ({
      count: prevState.count + 1
    }))
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">欢迎你的到来</h1>
        </header>
        <p className="App-intro">
          开始, 编辑 <code>src/App.js</code> 然后保存会自动重载
        </p>
        <span>{this.state.count}</span>
        <button onClick={this.handleButton.bind(this)}>测试</button>
        <button onClick={this.handleButton2}>测试</button>
        <button onClick={this.handleClick}>测试</button>
      </div>
    )
  }
}

export default App
